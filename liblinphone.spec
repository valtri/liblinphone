Name:           liblinphone
Version:        4.5.17
Release:        1%{?dist}
Summary:        High-level SIP library for linphone

License:        GPLv3+
URL:            https://github.com/BelledonneCommunications/%{name}
Source0:        https://github.com/BelledonneCommunications/%{name}/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz
# remove rpath, not intended for upstream
Patch0:         %{name}-rpath.patch

BuildRequires:  bctoolbox-devel
BuildRequires:  belle-sip-devel
BuildRequires:  belr-devel
BuildRequires:  belcard-devel
BuildRequires:  boost-devel
BuildRequires:  cmake
BuildRequires:  doxygen
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  libxml2-devel
BuildRequires:  mediastreamer2-devel
BuildRequires:  ortp-devel
BuildRequires:  python3-pip
#TODO: BuildRequires:  python3-pystache
BuildRequires:  python3-setuptools
BuildRequires:  python3-six
BuildRequires:  soci-devel
BuildRequires:  soci-mysql-devel
BuildRequires:  soci-sqlite3-devel
BuildRequires:  xerces-c-devel
BuildRequires:  zlib-devel

%description
Liblinphone is a high-level SIP library integrating all calling and instant
messaging features into an unified easy-to-use API. It is the cross-platform
VoIP library on which the *Linphone[1]* application is based on, and that
anyone can use to add audio and video calls or instant messaging capabilities
to an application.


%package        devel
Summary:        Development libraries for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
Libraries and headers required to develop software with %{name}.


%prep
%autosetup -p1


%build
# TODO: missing dependency pystache
pip install --user pystache
# TODO: missing dependencies lime
%cmake -DENABLE_STATIC=OFF -DENABLE_LIME=OFF -DENABLE_LIME_X3DH=OFF
%cmake_build


%install
%cmake_install
mv -v %{buildroot}%{_docdir}/liblinphone* %{buildroot}%{_pkgdocdir}


%ldconfig_scriptlets


%files
%license LICENSE.txt
%doc CHANGELOG.md
%doc README.md
%{_bindir}/*
%{_libdir}/liblinphone.so.10
%{_libdir}/liblinphone++.so.10
%{_datadir}/belr/grammars/*_grammar
%{_datadir}/linphone/
%{_datadir}/sounds/linphone/

%files devel
%doc %{_pkgdocdir}
%{_datadir}/Linphone/
%{_datadir}/LinphoneCxx/
%{_datadir}/liblinphone_tester/
%{_includedir}/linphone/
%{_includedir}/linphone++/
%{_libdir}/liblinphone.so
%{_libdir}/liblinphone++.so


%changelog
